import { IUtilisateur } from 'app/entities/utilisateur/utilisateur.model';
import { IPanier } from 'app/entities/panier/panier.model';

export interface ICoursier {
  id: number;
  nbcourse?: number | null;
  secteur?: string | null;
  utilisateur?: Pick<IUtilisateur, 'id'> | null;
  panier?: Pick<IPanier, 'id'> | null;
}

export type NewCoursier = Omit<ICoursier, 'id'> & { id: null };
