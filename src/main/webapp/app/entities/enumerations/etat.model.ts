export enum Etat {
  En_cours_de_preparation = 'En_cours_de_preparation',

  En_cours_de_livraison = 'En_cours_de_livraison',

  Prete = 'Prete',
}
