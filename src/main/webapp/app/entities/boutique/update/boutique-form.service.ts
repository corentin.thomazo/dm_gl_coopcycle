import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IBoutique, NewBoutique } from '../boutique.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IBoutique for edit and NewBoutiqueFormGroupInput for create.
 */
type BoutiqueFormGroupInput = IBoutique | PartialWithRequiredKeyOf<NewBoutique>;

type BoutiqueFormDefaults = Pick<NewBoutique, 'id' | 'cooperatives'>;

type BoutiqueFormGroupContent = {
  id: FormControl<IBoutique['id'] | NewBoutique['id']>;
  name: FormControl<IBoutique['name']>;
  lien: FormControl<IBoutique['lien']>;
  horairesOuverture: FormControl<IBoutique['horairesOuverture']>;
  thematique: FormControl<IBoutique['thematique']>;
  cooperatives: FormControl<IBoutique['cooperatives']>;
  utilisateur: FormControl<IBoutique['utilisateur']>;
};

export type BoutiqueFormGroup = FormGroup<BoutiqueFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class BoutiqueFormService {
  createBoutiqueFormGroup(boutique: BoutiqueFormGroupInput = { id: null }): BoutiqueFormGroup {
    const boutiqueRawValue = {
      ...this.getFormDefaults(),
      ...boutique,
    };
    return new FormGroup<BoutiqueFormGroupContent>({
      id: new FormControl(
        { value: boutiqueRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      name: new FormControl(boutiqueRawValue.name, {
        validators: [Validators.required],
      }),
      lien: new FormControl(boutiqueRawValue.lien, {
        validators: [Validators.required],
      }),
      horairesOuverture: new FormControl(boutiqueRawValue.horairesOuverture),
      thematique: new FormControl(boutiqueRawValue.thematique, {
        validators: [Validators.required],
      }),
      cooperatives: new FormControl(boutiqueRawValue.cooperatives ?? []),
      utilisateur: new FormControl(boutiqueRawValue.utilisateur),
    });
  }

  getBoutique(form: BoutiqueFormGroup): IBoutique | NewBoutique {
    return form.getRawValue() as IBoutique | NewBoutique;
  }

  resetForm(form: BoutiqueFormGroup, boutique: BoutiqueFormGroupInput): void {
    const boutiqueRawValue = { ...this.getFormDefaults(), ...boutique };
    form.reset(
      {
        ...boutiqueRawValue,
        id: { value: boutiqueRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): BoutiqueFormDefaults {
    return {
      id: null,
      cooperatives: [],
    };
  }
}
