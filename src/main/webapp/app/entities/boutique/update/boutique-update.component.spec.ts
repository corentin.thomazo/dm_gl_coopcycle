import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { BoutiqueFormService } from './boutique-form.service';
import { BoutiqueService } from '../service/boutique.service';
import { IBoutique } from '../boutique.model';
import { ICooperative } from 'app/entities/cooperative/cooperative.model';
import { CooperativeService } from 'app/entities/cooperative/service/cooperative.service';
import { IUtilisateur } from 'app/entities/utilisateur/utilisateur.model';
import { UtilisateurService } from 'app/entities/utilisateur/service/utilisateur.service';

import { BoutiqueUpdateComponent } from './boutique-update.component';

describe('Boutique Management Update Component', () => {
  let comp: BoutiqueUpdateComponent;
  let fixture: ComponentFixture<BoutiqueUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let boutiqueFormService: BoutiqueFormService;
  let boutiqueService: BoutiqueService;
  let cooperativeService: CooperativeService;
  let utilisateurService: UtilisateurService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [BoutiqueUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(BoutiqueUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(BoutiqueUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    boutiqueFormService = TestBed.inject(BoutiqueFormService);
    boutiqueService = TestBed.inject(BoutiqueService);
    cooperativeService = TestBed.inject(CooperativeService);
    utilisateurService = TestBed.inject(UtilisateurService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Cooperative query and add missing value', () => {
      const boutique: IBoutique = { id: 456 };
      const cooperatives: ICooperative[] = [{ id: 6308 }];
      boutique.cooperatives = cooperatives;

      const cooperativeCollection: ICooperative[] = [{ id: 19579 }];
      jest.spyOn(cooperativeService, 'query').mockReturnValue(of(new HttpResponse({ body: cooperativeCollection })));
      const additionalCooperatives = [...cooperatives];
      const expectedCollection: ICooperative[] = [...additionalCooperatives, ...cooperativeCollection];
      jest.spyOn(cooperativeService, 'addCooperativeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ boutique });
      comp.ngOnInit();

      expect(cooperativeService.query).toHaveBeenCalled();
      expect(cooperativeService.addCooperativeToCollectionIfMissing).toHaveBeenCalledWith(
        cooperativeCollection,
        ...additionalCooperatives.map(expect.objectContaining)
      );
      expect(comp.cooperativesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Utilisateur query and add missing value', () => {
      const boutique: IBoutique = { id: 456 };
      const utilisateur: IUtilisateur = { id: 89286 };
      boutique.utilisateur = utilisateur;

      const utilisateurCollection: IUtilisateur[] = [{ id: 46763 }];
      jest.spyOn(utilisateurService, 'query').mockReturnValue(of(new HttpResponse({ body: utilisateurCollection })));
      const additionalUtilisateurs = [utilisateur];
      const expectedCollection: IUtilisateur[] = [...additionalUtilisateurs, ...utilisateurCollection];
      jest.spyOn(utilisateurService, 'addUtilisateurToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ boutique });
      comp.ngOnInit();

      expect(utilisateurService.query).toHaveBeenCalled();
      expect(utilisateurService.addUtilisateurToCollectionIfMissing).toHaveBeenCalledWith(
        utilisateurCollection,
        ...additionalUtilisateurs.map(expect.objectContaining)
      );
      expect(comp.utilisateursSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const boutique: IBoutique = { id: 456 };
      const cooperative: ICooperative = { id: 62875 };
      boutique.cooperatives = [cooperative];
      const utilisateur: IUtilisateur = { id: 53731 };
      boutique.utilisateur = utilisateur;

      activatedRoute.data = of({ boutique });
      comp.ngOnInit();

      expect(comp.cooperativesSharedCollection).toContain(cooperative);
      expect(comp.utilisateursSharedCollection).toContain(utilisateur);
      expect(comp.boutique).toEqual(boutique);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IBoutique>>();
      const boutique = { id: 123 };
      jest.spyOn(boutiqueFormService, 'getBoutique').mockReturnValue(boutique);
      jest.spyOn(boutiqueService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ boutique });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: boutique }));
      saveSubject.complete();

      // THEN
      expect(boutiqueFormService.getBoutique).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(boutiqueService.update).toHaveBeenCalledWith(expect.objectContaining(boutique));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IBoutique>>();
      const boutique = { id: 123 };
      jest.spyOn(boutiqueFormService, 'getBoutique').mockReturnValue({ id: null });
      jest.spyOn(boutiqueService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ boutique: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: boutique }));
      saveSubject.complete();

      // THEN
      expect(boutiqueFormService.getBoutique).toHaveBeenCalled();
      expect(boutiqueService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IBoutique>>();
      const boutique = { id: 123 };
      jest.spyOn(boutiqueService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ boutique });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(boutiqueService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareCooperative', () => {
      it('Should forward to cooperativeService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(cooperativeService, 'compareCooperative');
        comp.compareCooperative(entity, entity2);
        expect(cooperativeService.compareCooperative).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareUtilisateur', () => {
      it('Should forward to utilisateurService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(utilisateurService, 'compareUtilisateur');
        comp.compareUtilisateur(entity, entity2);
        expect(utilisateurService.compareUtilisateur).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
