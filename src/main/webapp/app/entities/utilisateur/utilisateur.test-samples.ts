import { Role } from 'app/entities/enumerations/role.model';

import { IUtilisateur, NewUtilisateur } from './utilisateur.model';

export const sampleWithRequiredData: IUtilisateur = {
  id: 52583,
  name: 'Franche-Comté deposit Richelieu',
  adresse: 'Uruguayo Indonésie',
  telephone: '0362294015',
  mail: 'intangible',
};

export const sampleWithPartialData: IUtilisateur = {
  id: 8584,
  name: 'morph matrix',
  adresse: 'Specialiste Ingenieur',
  telephone: '+33 276593086',
  mail: 'Handcrafted c',
};

export const sampleWithFullData: IUtilisateur = {
  role: Role['Client'],
  id: 90924,
  name: 'Garden',
  adresse: 'Manager',
  telephone: '0330415039',
  mail: 'Sports',
};

export const sampleWithNewData: NewUtilisateur = {
  name: 'Small invoice',
  adresse: 'Computers compressing',
  telephone: '0342276643',
  mail: 'a parsing Soap',
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
