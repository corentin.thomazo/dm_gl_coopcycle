package polytech.info.gl.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import polytech.info.gl.domain.enumeration.Etat;

/**
 * A Panier.
 */
@Entity
@Table(name = "panier")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Panier implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id", nullable = false, unique = true)
    private Long id;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "valeur")
    private Long valeur;

    @Enumerated(EnumType.STRING)
    @Column(name = "state")
    private Etat state;

    @OneToMany(mappedBy = "panier")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "utilisateur", "panier" }, allowSetters = true)
    private Set<Coursier> coursiers = new HashSet<>();

    @ManyToMany
    @JoinTable(
        name = "rel_panier__produit",
        joinColumns = @JoinColumn(name = "panier_id"),
        inverseJoinColumns = @JoinColumn(name = "produit_id")
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "boutique", "paniers" }, allowSetters = true)
    private Set<Produit> produits = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "paniers", "coursiers", "boutiques", "cooperatives" }, allowSetters = true)
    private Utilisateur utilisateur;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Panier id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return this.date;
    }

    public Panier date(LocalDate date) {
        this.setDate(date);
        return this;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Long getValeur() {
        return this.valeur;
    }

    public Panier valeur(Long valeur) {
        this.setValeur(valeur);
        return this;
    }

    public void setValeur(Long valeur) {
        this.valeur = valeur;
    }

    public Etat getState() {
        return this.state;
    }

    public Panier state(Etat state) {
        this.setState(state);
        return this;
    }

    public void setState(Etat state) {
        this.state = state;
    }

    public Set<Coursier> getCoursiers() {
        return this.coursiers;
    }

    public void setCoursiers(Set<Coursier> coursiers) {
        if (this.coursiers != null) {
            this.coursiers.forEach(i -> i.setPanier(null));
        }
        if (coursiers != null) {
            coursiers.forEach(i -> i.setPanier(this));
        }
        this.coursiers = coursiers;
    }

    public Panier coursiers(Set<Coursier> coursiers) {
        this.setCoursiers(coursiers);
        return this;
    }

    public Panier addCoursier(Coursier coursier) {
        this.coursiers.add(coursier);
        coursier.setPanier(this);
        return this;
    }

    public Panier removeCoursier(Coursier coursier) {
        this.coursiers.remove(coursier);
        coursier.setPanier(null);
        return this;
    }

    public Set<Produit> getProduits() {
        return this.produits;
    }

    public void setProduits(Set<Produit> produits) {
        this.produits = produits;
    }

    public Panier produits(Set<Produit> produits) {
        this.setProduits(produits);
        return this;
    }

    public Panier addProduit(Produit produit) {
        this.produits.add(produit);
        produit.getPaniers().add(this);
        return this;
    }

    public Panier removeProduit(Produit produit) {
        this.produits.remove(produit);
        produit.getPaniers().remove(this);
        return this;
    }

    public Utilisateur getUtilisateur() {
        return this.utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public Panier utilisateur(Utilisateur utilisateur) {
        this.setUtilisateur(utilisateur);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Panier)) {
            return false;
        }
        return id != null && id.equals(((Panier) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Panier{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", valeur=" + getValeur() +
            ", state='" + getState() + "'" +
            "}";
    }
}
