import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../boutique.test-samples';

import { BoutiqueFormService } from './boutique-form.service';

describe('Boutique Form Service', () => {
  let service: BoutiqueFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BoutiqueFormService);
  });

  describe('Service methods', () => {
    describe('createBoutiqueFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createBoutiqueFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            name: expect.any(Object),
            lien: expect.any(Object),
            horairesOuverture: expect.any(Object),
            thematique: expect.any(Object),
            cooperatives: expect.any(Object),
            utilisateur: expect.any(Object),
          })
        );
      });

      it('passing IBoutique should create a new form with FormGroup', () => {
        const formGroup = service.createBoutiqueFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            name: expect.any(Object),
            lien: expect.any(Object),
            horairesOuverture: expect.any(Object),
            thematique: expect.any(Object),
            cooperatives: expect.any(Object),
            utilisateur: expect.any(Object),
          })
        );
      });
    });

    describe('getBoutique', () => {
      it('should return NewBoutique for default Boutique initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createBoutiqueFormGroup(sampleWithNewData);

        const boutique = service.getBoutique(formGroup) as any;

        expect(boutique).toMatchObject(sampleWithNewData);
      });

      it('should return NewBoutique for empty Boutique initial value', () => {
        const formGroup = service.createBoutiqueFormGroup();

        const boutique = service.getBoutique(formGroup) as any;

        expect(boutique).toMatchObject({});
      });

      it('should return IBoutique', () => {
        const formGroup = service.createBoutiqueFormGroup(sampleWithRequiredData);

        const boutique = service.getBoutique(formGroup) as any;

        expect(boutique).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IBoutique should not enable id FormControl', () => {
        const formGroup = service.createBoutiqueFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewBoutique should disable id FormControl', () => {
        const formGroup = service.createBoutiqueFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
