import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { BoutiqueComponent } from '../list/boutique.component';
import { BoutiqueDetailComponent } from '../detail/boutique-detail.component';
import { BoutiqueUpdateComponent } from '../update/boutique-update.component';
import { BoutiqueRoutingResolveService } from './boutique-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const boutiqueRoute: Routes = [
  {
    path: '',
    component: BoutiqueComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: BoutiqueDetailComponent,
    resolve: {
      boutique: BoutiqueRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: BoutiqueUpdateComponent,
    resolve: {
      boutique: BoutiqueRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: BoutiqueUpdateComponent,
    resolve: {
      boutique: BoutiqueRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(boutiqueRoute)],
  exports: [RouterModule],
})
export class BoutiqueRoutingModule {}
