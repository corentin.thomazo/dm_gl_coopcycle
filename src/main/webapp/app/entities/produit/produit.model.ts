import { IBoutique } from 'app/entities/boutique/boutique.model';
import { IPanier } from 'app/entities/panier/panier.model';

export interface IProduit {
  id: number;
  intitule?: number | null;
  prix?: number | null;
  boutique?: Pick<IBoutique, 'id'> | null;
  paniers?: Pick<IPanier, 'id'>[] | null;
}

export type NewProduit = Omit<IProduit, 'id'> & { id: null };
