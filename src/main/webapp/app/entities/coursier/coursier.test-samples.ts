import { ICoursier, NewCoursier } from './coursier.model';

export const sampleWithRequiredData: ICoursier = {
  id: 64789,
};

export const sampleWithPartialData: ICoursier = {
  id: 47218,
  nbcourse: 67243,
};

export const sampleWithFullData: ICoursier = {
  id: 71307,
  nbcourse: 59545,
  secteur: 'Venezuela',
};

export const sampleWithNewData: NewCoursier = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
