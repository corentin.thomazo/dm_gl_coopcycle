import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { BoutiqueFormService, BoutiqueFormGroup } from './boutique-form.service';
import { IBoutique } from '../boutique.model';
import { BoutiqueService } from '../service/boutique.service';
import { ICooperative } from 'app/entities/cooperative/cooperative.model';
import { CooperativeService } from 'app/entities/cooperative/service/cooperative.service';
import { IUtilisateur } from 'app/entities/utilisateur/utilisateur.model';
import { UtilisateurService } from 'app/entities/utilisateur/service/utilisateur.service';

@Component({
  selector: 'jhi-boutique-update',
  templateUrl: './boutique-update.component.html',
})
export class BoutiqueUpdateComponent implements OnInit {
  isSaving = false;
  boutique: IBoutique | null = null;

  cooperativesSharedCollection: ICooperative[] = [];
  utilisateursSharedCollection: IUtilisateur[] = [];

  editForm: BoutiqueFormGroup = this.boutiqueFormService.createBoutiqueFormGroup();

  constructor(
    protected boutiqueService: BoutiqueService,
    protected boutiqueFormService: BoutiqueFormService,
    protected cooperativeService: CooperativeService,
    protected utilisateurService: UtilisateurService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareCooperative = (o1: ICooperative | null, o2: ICooperative | null): boolean => this.cooperativeService.compareCooperative(o1, o2);

  compareUtilisateur = (o1: IUtilisateur | null, o2: IUtilisateur | null): boolean => this.utilisateurService.compareUtilisateur(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ boutique }) => {
      this.boutique = boutique;
      if (boutique) {
        this.updateForm(boutique);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const boutique = this.boutiqueFormService.getBoutique(this.editForm);
    if (boutique.id !== null) {
      this.subscribeToSaveResponse(this.boutiqueService.update(boutique));
    } else {
      this.subscribeToSaveResponse(this.boutiqueService.create(boutique));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBoutique>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(boutique: IBoutique): void {
    this.boutique = boutique;
    this.boutiqueFormService.resetForm(this.editForm, boutique);

    this.cooperativesSharedCollection = this.cooperativeService.addCooperativeToCollectionIfMissing<ICooperative>(
      this.cooperativesSharedCollection,
      ...(boutique.cooperatives ?? [])
    );
    this.utilisateursSharedCollection = this.utilisateurService.addUtilisateurToCollectionIfMissing<IUtilisateur>(
      this.utilisateursSharedCollection,
      boutique.utilisateur
    );
  }

  protected loadRelationshipsOptions(): void {
    this.cooperativeService
      .query()
      .pipe(map((res: HttpResponse<ICooperative[]>) => res.body ?? []))
      .pipe(
        map((cooperatives: ICooperative[]) =>
          this.cooperativeService.addCooperativeToCollectionIfMissing<ICooperative>(cooperatives, ...(this.boutique?.cooperatives ?? []))
        )
      )
      .subscribe((cooperatives: ICooperative[]) => (this.cooperativesSharedCollection = cooperatives));

    this.utilisateurService
      .query()
      .pipe(map((res: HttpResponse<IUtilisateur[]>) => res.body ?? []))
      .pipe(
        map((utilisateurs: IUtilisateur[]) =>
          this.utilisateurService.addUtilisateurToCollectionIfMissing<IUtilisateur>(utilisateurs, this.boutique?.utilisateur)
        )
      )
      .subscribe((utilisateurs: IUtilisateur[]) => (this.utilisateursSharedCollection = utilisateurs));
  }
}
