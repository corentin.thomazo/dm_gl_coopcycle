package polytech.info.gl.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import polytech.info.gl.domain.enumeration.Role;

/**
 * A Utilisateur.
 */
@Entity
@Table(name = "utilisateur")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Utilisateur implements Serializable {

    private static final long serialVersionUID = 1L;

    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    private Role role;

    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id", nullable = false, unique = true)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "adresse", nullable = false)
    private String adresse;

    @NotNull
    @Column(name = "telephone", nullable = false)
    private String telephone;

    @NotNull
    @Column(name = "mail", nullable = false)
    private String mail;

    @OneToMany(mappedBy = "utilisateur")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "coursiers", "produits", "utilisateur" }, allowSetters = true)
    private Set<Panier> paniers = new HashSet<>();

    @OneToMany(mappedBy = "utilisateur")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "utilisateur", "panier" }, allowSetters = true)
    private Set<Coursier> coursiers = new HashSet<>();

    @OneToMany(mappedBy = "utilisateur")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "produits", "cooperatives", "utilisateur" }, allowSetters = true)
    private Set<Boutique> boutiques = new HashSet<>();

    @ManyToMany
    @JoinTable(
        name = "rel_utilisateur__cooperative",
        joinColumns = @JoinColumn(name = "utilisateur_id"),
        inverseJoinColumns = @JoinColumn(name = "cooperative_id")
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "utilisateurs", "boutiques" }, allowSetters = true)
    private Set<Cooperative> cooperatives = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Role getRole() {
        return this.role;
    }

    public Utilisateur role(Role role) {
        this.setRole(role);
        return this;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Long getId() {
        return this.id;
    }

    public Utilisateur id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Utilisateur name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdresse() {
        return this.adresse;
    }

    public Utilisateur adresse(String adresse) {
        this.setAdresse(adresse);
        return this;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getTelephone() {
        return this.telephone;
    }

    public Utilisateur telephone(String telephone) {
        this.setTelephone(telephone);
        return this;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getMail() {
        return this.mail;
    }

    public Utilisateur mail(String mail) {
        this.setMail(mail);
        return this;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Set<Panier> getPaniers() {
        return this.paniers;
    }

    public void setPaniers(Set<Panier> paniers) {
        if (this.paniers != null) {
            this.paniers.forEach(i -> i.setUtilisateur(null));
        }
        if (paniers != null) {
            paniers.forEach(i -> i.setUtilisateur(this));
        }
        this.paniers = paniers;
    }

    public Utilisateur paniers(Set<Panier> paniers) {
        this.setPaniers(paniers);
        return this;
    }

    public Utilisateur addPanier(Panier panier) {
        this.paniers.add(panier);
        panier.setUtilisateur(this);
        return this;
    }

    public Utilisateur removePanier(Panier panier) {
        this.paniers.remove(panier);
        panier.setUtilisateur(null);
        return this;
    }

    public Set<Coursier> getCoursiers() {
        return this.coursiers;
    }

    public void setCoursiers(Set<Coursier> coursiers) {
        if (this.coursiers != null) {
            this.coursiers.forEach(i -> i.setUtilisateur(null));
        }
        if (coursiers != null) {
            coursiers.forEach(i -> i.setUtilisateur(this));
        }
        this.coursiers = coursiers;
    }

    public Utilisateur coursiers(Set<Coursier> coursiers) {
        this.setCoursiers(coursiers);
        return this;
    }

    public Utilisateur addCoursier(Coursier coursier) {
        this.coursiers.add(coursier);
        coursier.setUtilisateur(this);
        return this;
    }

    public Utilisateur removeCoursier(Coursier coursier) {
        this.coursiers.remove(coursier);
        coursier.setUtilisateur(null);
        return this;
    }

    public Set<Boutique> getBoutiques() {
        return this.boutiques;
    }

    public void setBoutiques(Set<Boutique> boutiques) {
        if (this.boutiques != null) {
            this.boutiques.forEach(i -> i.setUtilisateur(null));
        }
        if (boutiques != null) {
            boutiques.forEach(i -> i.setUtilisateur(this));
        }
        this.boutiques = boutiques;
    }

    public Utilisateur boutiques(Set<Boutique> boutiques) {
        this.setBoutiques(boutiques);
        return this;
    }

    public Utilisateur addBoutique(Boutique boutique) {
        this.boutiques.add(boutique);
        boutique.setUtilisateur(this);
        return this;
    }

    public Utilisateur removeBoutique(Boutique boutique) {
        this.boutiques.remove(boutique);
        boutique.setUtilisateur(null);
        return this;
    }

    public Set<Cooperative> getCooperatives() {
        return this.cooperatives;
    }

    public void setCooperatives(Set<Cooperative> cooperatives) {
        this.cooperatives = cooperatives;
    }

    public Utilisateur cooperatives(Set<Cooperative> cooperatives) {
        this.setCooperatives(cooperatives);
        return this;
    }

    public Utilisateur addCooperative(Cooperative cooperative) {
        this.cooperatives.add(cooperative);
        cooperative.getUtilisateurs().add(this);
        return this;
    }

    public Utilisateur removeCooperative(Cooperative cooperative) {
        this.cooperatives.remove(cooperative);
        cooperative.getUtilisateurs().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Utilisateur)) {
            return false;
        }
        return id != null && id.equals(((Utilisateur) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Utilisateur{" +
            "id=" + getId() +
            ", role='" + getRole() + "'" +
            ", name='" + getName() + "'" +
            ", adresse='" + getAdresse() + "'" +
            ", telephone='" + getTelephone() + "'" +
            ", mail='" + getMail() + "'" +
            "}";
    }
}
