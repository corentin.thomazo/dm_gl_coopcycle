import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { ICoursier, NewCoursier } from '../coursier.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts ICoursier for edit and NewCoursierFormGroupInput for create.
 */
type CoursierFormGroupInput = ICoursier | PartialWithRequiredKeyOf<NewCoursier>;

type CoursierFormDefaults = Pick<NewCoursier, 'id'>;

type CoursierFormGroupContent = {
  id: FormControl<ICoursier['id'] | NewCoursier['id']>;
  nbcourse: FormControl<ICoursier['nbcourse']>;
  secteur: FormControl<ICoursier['secteur']>;
  utilisateur: FormControl<ICoursier['utilisateur']>;
  panier: FormControl<ICoursier['panier']>;
};

export type CoursierFormGroup = FormGroup<CoursierFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class CoursierFormService {
  createCoursierFormGroup(coursier: CoursierFormGroupInput = { id: null }): CoursierFormGroup {
    const coursierRawValue = {
      ...this.getFormDefaults(),
      ...coursier,
    };
    return new FormGroup<CoursierFormGroupContent>({
      id: new FormControl(
        { value: coursierRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      nbcourse: new FormControl(coursierRawValue.nbcourse),
      secteur: new FormControl(coursierRawValue.secteur),
      utilisateur: new FormControl(coursierRawValue.utilisateur),
      panier: new FormControl(coursierRawValue.panier),
    });
  }

  getCoursier(form: CoursierFormGroup): ICoursier | NewCoursier {
    return form.getRawValue() as ICoursier | NewCoursier;
  }

  resetForm(form: CoursierFormGroup, coursier: CoursierFormGroupInput): void {
    const coursierRawValue = { ...this.getFormDefaults(), ...coursier };
    form.reset(
      {
        ...coursierRawValue,
        id: { value: coursierRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): CoursierFormDefaults {
    return {
      id: null,
    };
  }
}
