import { IProduit, NewProduit } from './produit.model';

export const sampleWithRequiredData: IProduit = {
  id: 91013,
  intitule: 16294,
  prix: 1691,
};

export const sampleWithPartialData: IProduit = {
  id: 35144,
  intitule: 62571,
  prix: 46320,
};

export const sampleWithFullData: IProduit = {
  id: 55887,
  intitule: 63349,
  prix: 26916,
};

export const sampleWithNewData: NewProduit = {
  intitule: 18350,
  prix: 28265,
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
