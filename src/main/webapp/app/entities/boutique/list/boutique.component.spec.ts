import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { BoutiqueService } from '../service/boutique.service';

import { BoutiqueComponent } from './boutique.component';

describe('Boutique Management Component', () => {
  let comp: BoutiqueComponent;
  let fixture: ComponentFixture<BoutiqueComponent>;
  let service: BoutiqueService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([{ path: 'boutique', component: BoutiqueComponent }]), HttpClientTestingModule],
      declarations: [BoutiqueComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              defaultSort: 'id,asc',
            }),
            queryParamMap: of(
              jest.requireActual('@angular/router').convertToParamMap({
                page: '1',
                size: '1',
                sort: 'id,desc',
              })
            ),
            snapshot: { queryParams: {} },
          },
        },
      ],
    })
      .overrideTemplate(BoutiqueComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(BoutiqueComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(BoutiqueService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.boutiques?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });

  describe('trackId', () => {
    it('Should forward to boutiqueService', () => {
      const entity = { id: 123 };
      jest.spyOn(service, 'getBoutiqueIdentifier');
      const id = comp.trackId(0, entity);
      expect(service.getBoutiqueIdentifier).toHaveBeenCalledWith(entity);
      expect(id).toBe(entity.id);
    });
  });
});
