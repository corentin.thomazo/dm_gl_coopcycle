import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { CoursierFormService, CoursierFormGroup } from './coursier-form.service';
import { ICoursier } from '../coursier.model';
import { CoursierService } from '../service/coursier.service';
import { IUtilisateur } from 'app/entities/utilisateur/utilisateur.model';
import { UtilisateurService } from 'app/entities/utilisateur/service/utilisateur.service';
import { IPanier } from 'app/entities/panier/panier.model';
import { PanierService } from 'app/entities/panier/service/panier.service';

@Component({
  selector: 'jhi-coursier-update',
  templateUrl: './coursier-update.component.html',
})
export class CoursierUpdateComponent implements OnInit {
  isSaving = false;
  coursier: ICoursier | null = null;

  utilisateursSharedCollection: IUtilisateur[] = [];
  paniersSharedCollection: IPanier[] = [];

  editForm: CoursierFormGroup = this.coursierFormService.createCoursierFormGroup();

  constructor(
    protected coursierService: CoursierService,
    protected coursierFormService: CoursierFormService,
    protected utilisateurService: UtilisateurService,
    protected panierService: PanierService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareUtilisateur = (o1: IUtilisateur | null, o2: IUtilisateur | null): boolean => this.utilisateurService.compareUtilisateur(o1, o2);

  comparePanier = (o1: IPanier | null, o2: IPanier | null): boolean => this.panierService.comparePanier(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ coursier }) => {
      this.coursier = coursier;
      if (coursier) {
        this.updateForm(coursier);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const coursier = this.coursierFormService.getCoursier(this.editForm);
    if (coursier.id !== null) {
      this.subscribeToSaveResponse(this.coursierService.update(coursier));
    } else {
      this.subscribeToSaveResponse(this.coursierService.create(coursier));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICoursier>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(coursier: ICoursier): void {
    this.coursier = coursier;
    this.coursierFormService.resetForm(this.editForm, coursier);

    this.utilisateursSharedCollection = this.utilisateurService.addUtilisateurToCollectionIfMissing<IUtilisateur>(
      this.utilisateursSharedCollection,
      coursier.utilisateur
    );
    this.paniersSharedCollection = this.panierService.addPanierToCollectionIfMissing<IPanier>(
      this.paniersSharedCollection,
      coursier.panier
    );
  }

  protected loadRelationshipsOptions(): void {
    this.utilisateurService
      .query()
      .pipe(map((res: HttpResponse<IUtilisateur[]>) => res.body ?? []))
      .pipe(
        map((utilisateurs: IUtilisateur[]) =>
          this.utilisateurService.addUtilisateurToCollectionIfMissing<IUtilisateur>(utilisateurs, this.coursier?.utilisateur)
        )
      )
      .subscribe((utilisateurs: IUtilisateur[]) => (this.utilisateursSharedCollection = utilisateurs));

    this.panierService
      .query()
      .pipe(map((res: HttpResponse<IPanier[]>) => res.body ?? []))
      .pipe(map((paniers: IPanier[]) => this.panierService.addPanierToCollectionIfMissing<IPanier>(paniers, this.coursier?.panier)))
      .subscribe((paniers: IPanier[]) => (this.paniersSharedCollection = paniers));
  }
}
