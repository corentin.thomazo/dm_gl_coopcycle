package polytech.info.gl.domain.enumeration;

/**
 * The Role enumeration.
 */
public enum Role {
    Client,
    Societaire,
    DirecteurGeneral,
    Commercant,
    Coursier,
}
