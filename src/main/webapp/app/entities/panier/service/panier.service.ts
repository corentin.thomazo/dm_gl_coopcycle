import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IPanier, NewPanier } from '../panier.model';

export type PartialUpdatePanier = Partial<IPanier> & Pick<IPanier, 'id'>;

type RestOf<T extends IPanier | NewPanier> = Omit<T, 'date'> & {
  date?: string | null;
};

export type RestPanier = RestOf<IPanier>;

export type NewRestPanier = RestOf<NewPanier>;

export type PartialUpdateRestPanier = RestOf<PartialUpdatePanier>;

export type EntityResponseType = HttpResponse<IPanier>;
export type EntityArrayResponseType = HttpResponse<IPanier[]>;

@Injectable({ providedIn: 'root' })
export class PanierService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/paniers');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(panier: NewPanier): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(panier);
    return this.http
      .post<RestPanier>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(panier: IPanier): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(panier);
    return this.http
      .put<RestPanier>(`${this.resourceUrl}/${this.getPanierIdentifier(panier)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(panier: PartialUpdatePanier): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(panier);
    return this.http
      .patch<RestPanier>(`${this.resourceUrl}/${this.getPanierIdentifier(panier)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestPanier>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestPanier[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getPanierIdentifier(panier: Pick<IPanier, 'id'>): number {
    return panier.id;
  }

  comparePanier(o1: Pick<IPanier, 'id'> | null, o2: Pick<IPanier, 'id'> | null): boolean {
    return o1 && o2 ? this.getPanierIdentifier(o1) === this.getPanierIdentifier(o2) : o1 === o2;
  }

  addPanierToCollectionIfMissing<Type extends Pick<IPanier, 'id'>>(
    panierCollection: Type[],
    ...paniersToCheck: (Type | null | undefined)[]
  ): Type[] {
    const paniers: Type[] = paniersToCheck.filter(isPresent);
    if (paniers.length > 0) {
      const panierCollectionIdentifiers = panierCollection.map(panierItem => this.getPanierIdentifier(panierItem)!);
      const paniersToAdd = paniers.filter(panierItem => {
        const panierIdentifier = this.getPanierIdentifier(panierItem);
        if (panierCollectionIdentifiers.includes(panierIdentifier)) {
          return false;
        }
        panierCollectionIdentifiers.push(panierIdentifier);
        return true;
      });
      return [...paniersToAdd, ...panierCollection];
    }
    return panierCollection;
  }

  protected convertDateFromClient<T extends IPanier | NewPanier | PartialUpdatePanier>(panier: T): RestOf<T> {
    return {
      ...panier,
      date: panier.date?.format(DATE_FORMAT) ?? null,
    };
  }

  protected convertDateFromServer(restPanier: RestPanier): IPanier {
    return {
      ...restPanier,
      date: restPanier.date ? dayjs(restPanier.date) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestPanier>): HttpResponse<IPanier> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestPanier[]>): HttpResponse<IPanier[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
