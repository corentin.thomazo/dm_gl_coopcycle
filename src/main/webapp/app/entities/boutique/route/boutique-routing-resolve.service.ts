import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IBoutique } from '../boutique.model';
import { BoutiqueService } from '../service/boutique.service';

@Injectable({ providedIn: 'root' })
export class BoutiqueRoutingResolveService implements Resolve<IBoutique | null> {
  constructor(protected service: BoutiqueService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBoutique | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((boutique: HttpResponse<IBoutique>) => {
          if (boutique.body) {
            return of(boutique.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}
