import dayjs from 'dayjs/esm';
import { IProduit } from 'app/entities/produit/produit.model';
import { IUtilisateur } from 'app/entities/utilisateur/utilisateur.model';
import { Etat } from 'app/entities/enumerations/etat.model';

export interface IPanier {
  id: number;
  date?: dayjs.Dayjs | null;
  valeur?: number | null;
  state?: Etat | null;
  produits?: Pick<IProduit, 'id'>[] | null;
  utilisateur?: Pick<IUtilisateur, 'id'> | null;
}

export type NewPanier = Omit<IPanier, 'id'> & { id: null };
