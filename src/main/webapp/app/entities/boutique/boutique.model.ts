import { ICooperative } from 'app/entities/cooperative/cooperative.model';
import { IUtilisateur } from 'app/entities/utilisateur/utilisateur.model';

export interface IBoutique {
  id: number;
  name?: string | null;
  lien?: string | null;
  horairesOuverture?: string | null;
  thematique?: string | null;
  cooperatives?: Pick<ICooperative, 'id'>[] | null;
  utilisateur?: Pick<IUtilisateur, 'id'> | null;
}

export type NewBoutique = Omit<IBoutique, 'id'> & { id: null };
