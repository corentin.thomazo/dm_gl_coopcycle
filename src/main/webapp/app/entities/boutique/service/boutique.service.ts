import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IBoutique, NewBoutique } from '../boutique.model';

export type PartialUpdateBoutique = Partial<IBoutique> & Pick<IBoutique, 'id'>;

export type EntityResponseType = HttpResponse<IBoutique>;
export type EntityArrayResponseType = HttpResponse<IBoutique[]>;

@Injectable({ providedIn: 'root' })
export class BoutiqueService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/boutiques');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(boutique: NewBoutique): Observable<EntityResponseType> {
    return this.http.post<IBoutique>(this.resourceUrl, boutique, { observe: 'response' });
  }

  update(boutique: IBoutique): Observable<EntityResponseType> {
    return this.http.put<IBoutique>(`${this.resourceUrl}/${this.getBoutiqueIdentifier(boutique)}`, boutique, { observe: 'response' });
  }

  partialUpdate(boutique: PartialUpdateBoutique): Observable<EntityResponseType> {
    return this.http.patch<IBoutique>(`${this.resourceUrl}/${this.getBoutiqueIdentifier(boutique)}`, boutique, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IBoutique>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IBoutique[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getBoutiqueIdentifier(boutique: Pick<IBoutique, 'id'>): number {
    return boutique.id;
  }

  compareBoutique(o1: Pick<IBoutique, 'id'> | null, o2: Pick<IBoutique, 'id'> | null): boolean {
    return o1 && o2 ? this.getBoutiqueIdentifier(o1) === this.getBoutiqueIdentifier(o2) : o1 === o2;
  }

  addBoutiqueToCollectionIfMissing<Type extends Pick<IBoutique, 'id'>>(
    boutiqueCollection: Type[],
    ...boutiquesToCheck: (Type | null | undefined)[]
  ): Type[] {
    const boutiques: Type[] = boutiquesToCheck.filter(isPresent);
    if (boutiques.length > 0) {
      const boutiqueCollectionIdentifiers = boutiqueCollection.map(boutiqueItem => this.getBoutiqueIdentifier(boutiqueItem)!);
      const boutiquesToAdd = boutiques.filter(boutiqueItem => {
        const boutiqueIdentifier = this.getBoutiqueIdentifier(boutiqueItem);
        if (boutiqueCollectionIdentifiers.includes(boutiqueIdentifier)) {
          return false;
        }
        boutiqueCollectionIdentifiers.push(boutiqueIdentifier);
        return true;
      });
      return [...boutiquesToAdd, ...boutiqueCollection];
    }
    return boutiqueCollection;
  }
}
