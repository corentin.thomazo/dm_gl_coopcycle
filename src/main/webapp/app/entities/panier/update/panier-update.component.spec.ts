import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { PanierFormService } from './panier-form.service';
import { PanierService } from '../service/panier.service';
import { IPanier } from '../panier.model';
import { IProduit } from 'app/entities/produit/produit.model';
import { ProduitService } from 'app/entities/produit/service/produit.service';
import { IUtilisateur } from 'app/entities/utilisateur/utilisateur.model';
import { UtilisateurService } from 'app/entities/utilisateur/service/utilisateur.service';

import { PanierUpdateComponent } from './panier-update.component';

describe('Panier Management Update Component', () => {
  let comp: PanierUpdateComponent;
  let fixture: ComponentFixture<PanierUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let panierFormService: PanierFormService;
  let panierService: PanierService;
  let produitService: ProduitService;
  let utilisateurService: UtilisateurService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [PanierUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(PanierUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(PanierUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    panierFormService = TestBed.inject(PanierFormService);
    panierService = TestBed.inject(PanierService);
    produitService = TestBed.inject(ProduitService);
    utilisateurService = TestBed.inject(UtilisateurService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Produit query and add missing value', () => {
      const panier: IPanier = { id: 456 };
      const produits: IProduit[] = [{ id: 20161 }];
      panier.produits = produits;

      const produitCollection: IProduit[] = [{ id: 11736 }];
      jest.spyOn(produitService, 'query').mockReturnValue(of(new HttpResponse({ body: produitCollection })));
      const additionalProduits = [...produits];
      const expectedCollection: IProduit[] = [...additionalProduits, ...produitCollection];
      jest.spyOn(produitService, 'addProduitToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ panier });
      comp.ngOnInit();

      expect(produitService.query).toHaveBeenCalled();
      expect(produitService.addProduitToCollectionIfMissing).toHaveBeenCalledWith(
        produitCollection,
        ...additionalProduits.map(expect.objectContaining)
      );
      expect(comp.produitsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Utilisateur query and add missing value', () => {
      const panier: IPanier = { id: 456 };
      const utilisateur: IUtilisateur = { id: 74442 };
      panier.utilisateur = utilisateur;

      const utilisateurCollection: IUtilisateur[] = [{ id: 97642 }];
      jest.spyOn(utilisateurService, 'query').mockReturnValue(of(new HttpResponse({ body: utilisateurCollection })));
      const additionalUtilisateurs = [utilisateur];
      const expectedCollection: IUtilisateur[] = [...additionalUtilisateurs, ...utilisateurCollection];
      jest.spyOn(utilisateurService, 'addUtilisateurToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ panier });
      comp.ngOnInit();

      expect(utilisateurService.query).toHaveBeenCalled();
      expect(utilisateurService.addUtilisateurToCollectionIfMissing).toHaveBeenCalledWith(
        utilisateurCollection,
        ...additionalUtilisateurs.map(expect.objectContaining)
      );
      expect(comp.utilisateursSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const panier: IPanier = { id: 456 };
      const produit: IProduit = { id: 96873 };
      panier.produits = [produit];
      const utilisateur: IUtilisateur = { id: 12336 };
      panier.utilisateur = utilisateur;

      activatedRoute.data = of({ panier });
      comp.ngOnInit();

      expect(comp.produitsSharedCollection).toContain(produit);
      expect(comp.utilisateursSharedCollection).toContain(utilisateur);
      expect(comp.panier).toEqual(panier);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IPanier>>();
      const panier = { id: 123 };
      jest.spyOn(panierFormService, 'getPanier').mockReturnValue(panier);
      jest.spyOn(panierService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ panier });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: panier }));
      saveSubject.complete();

      // THEN
      expect(panierFormService.getPanier).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(panierService.update).toHaveBeenCalledWith(expect.objectContaining(panier));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IPanier>>();
      const panier = { id: 123 };
      jest.spyOn(panierFormService, 'getPanier').mockReturnValue({ id: null });
      jest.spyOn(panierService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ panier: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: panier }));
      saveSubject.complete();

      // THEN
      expect(panierFormService.getPanier).toHaveBeenCalled();
      expect(panierService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IPanier>>();
      const panier = { id: 123 };
      jest.spyOn(panierService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ panier });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(panierService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareProduit', () => {
      it('Should forward to produitService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(produitService, 'compareProduit');
        comp.compareProduit(entity, entity2);
        expect(produitService.compareProduit).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareUtilisateur', () => {
      it('Should forward to utilisateurService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(utilisateurService, 'compareUtilisateur');
        comp.compareUtilisateur(entity, entity2);
        expect(utilisateurService.compareUtilisateur).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
