import { IUtilisateur } from 'app/entities/utilisateur/utilisateur.model';
import { IBoutique } from 'app/entities/boutique/boutique.model';

export interface ICooperative {
  id: number;
  name?: string | null;
  utilisateurs?: Pick<IUtilisateur, 'id'>[] | null;
  boutiques?: Pick<IBoutique, 'id'>[] | null;
}

export type NewCooperative = Omit<ICooperative, 'id'> & { id: null };
