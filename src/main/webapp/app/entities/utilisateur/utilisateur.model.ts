import { ICooperative } from 'app/entities/cooperative/cooperative.model';
import { Role } from 'app/entities/enumerations/role.model';

export interface IUtilisateur {
  role?: Role | null;
  id: number;
  name?: string | null;
  adresse?: string | null;
  telephone?: string | null;
  mail?: string | null;
  cooperatives?: Pick<ICooperative, 'id'>[] | null;
}

export type NewUtilisateur = Omit<IUtilisateur, 'id'> & { id: null };
