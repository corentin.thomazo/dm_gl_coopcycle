import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IPanier, NewPanier } from '../panier.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IPanier for edit and NewPanierFormGroupInput for create.
 */
type PanierFormGroupInput = IPanier | PartialWithRequiredKeyOf<NewPanier>;

type PanierFormDefaults = Pick<NewPanier, 'id' | 'produits'>;

type PanierFormGroupContent = {
  id: FormControl<IPanier['id'] | NewPanier['id']>;
  date: FormControl<IPanier['date']>;
  valeur: FormControl<IPanier['valeur']>;
  state: FormControl<IPanier['state']>;
  produits: FormControl<IPanier['produits']>;
  utilisateur: FormControl<IPanier['utilisateur']>;
};

export type PanierFormGroup = FormGroup<PanierFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class PanierFormService {
  createPanierFormGroup(panier: PanierFormGroupInput = { id: null }): PanierFormGroup {
    const panierRawValue = {
      ...this.getFormDefaults(),
      ...panier,
    };
    return new FormGroup<PanierFormGroupContent>({
      id: new FormControl(
        { value: panierRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      date: new FormControl(panierRawValue.date),
      valeur: new FormControl(panierRawValue.valeur),
      state: new FormControl(panierRawValue.state),
      produits: new FormControl(panierRawValue.produits ?? []),
      utilisateur: new FormControl(panierRawValue.utilisateur),
    });
  }

  getPanier(form: PanierFormGroup): IPanier | NewPanier {
    return form.getRawValue() as IPanier | NewPanier;
  }

  resetForm(form: PanierFormGroup, panier: PanierFormGroupInput): void {
    const panierRawValue = { ...this.getFormDefaults(), ...panier };
    form.reset(
      {
        ...panierRawValue,
        id: { value: panierRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): PanierFormDefaults {
    return {
      id: null,
      produits: [],
    };
  }
}
