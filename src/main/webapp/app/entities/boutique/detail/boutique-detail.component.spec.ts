import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { BoutiqueDetailComponent } from './boutique-detail.component';

describe('Boutique Management Detail Component', () => {
  let comp: BoutiqueDetailComponent;
  let fixture: ComponentFixture<BoutiqueDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BoutiqueDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ boutique: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(BoutiqueDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(BoutiqueDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load boutique on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.boutique).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
