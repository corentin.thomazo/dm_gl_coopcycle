import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'utilisateur',
        data: { pageTitle: 'blogApp.utilisateur.home.title' },
        loadChildren: () => import('./utilisateur/utilisateur.module').then(m => m.UtilisateurModule),
      },
      {
        path: 'coursier',
        data: { pageTitle: 'blogApp.coursier.home.title' },
        loadChildren: () => import('./coursier/coursier.module').then(m => m.CoursierModule),
      },
      {
        path: 'panier',
        data: { pageTitle: 'blogApp.panier.home.title' },
        loadChildren: () => import('./panier/panier.module').then(m => m.PanierModule),
      },
      {
        path: 'produit',
        data: { pageTitle: 'blogApp.produit.home.title' },
        loadChildren: () => import('./produit/produit.module').then(m => m.ProduitModule),
      },
      {
        path: 'boutique',
        data: { pageTitle: 'blogApp.boutique.home.title' },
        loadChildren: () => import('./boutique/boutique.module').then(m => m.BoutiqueModule),
      },
      {
        path: 'cooperative',
        data: { pageTitle: 'blogApp.cooperative.home.title' },
        loadChildren: () => import('./cooperative/cooperative.module').then(m => m.CooperativeModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
