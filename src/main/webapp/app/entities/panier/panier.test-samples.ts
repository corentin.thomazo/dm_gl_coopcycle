import dayjs from 'dayjs/esm';

import { Etat } from 'app/entities/enumerations/etat.model';

import { IPanier, NewPanier } from './panier.model';

export const sampleWithRequiredData: IPanier = {
  id: 13332,
};

export const sampleWithPartialData: IPanier = {
  id: 76442,
  date: dayjs('2023-04-09'),
  state: Etat['En_cours_de_preparation'],
};

export const sampleWithFullData: IPanier = {
  id: 31280,
  date: dayjs('2023-04-09'),
  valeur: 53052,
  state: Etat['Prete'],
};

export const sampleWithNewData: NewPanier = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
