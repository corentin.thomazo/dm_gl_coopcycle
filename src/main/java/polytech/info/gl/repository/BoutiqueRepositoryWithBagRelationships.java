package polytech.info.gl.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import polytech.info.gl.domain.Boutique;

public interface BoutiqueRepositoryWithBagRelationships {
    Optional<Boutique> fetchBagRelationships(Optional<Boutique> boutique);

    List<Boutique> fetchBagRelationships(List<Boutique> boutiques);

    Page<Boutique> fetchBagRelationships(Page<Boutique> boutiques);
}
