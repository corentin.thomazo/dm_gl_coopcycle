import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { BoutiqueComponent } from './list/boutique.component';
import { BoutiqueDetailComponent } from './detail/boutique-detail.component';
import { BoutiqueUpdateComponent } from './update/boutique-update.component';
import { BoutiqueDeleteDialogComponent } from './delete/boutique-delete-dialog.component';
import { BoutiqueRoutingModule } from './route/boutique-routing.module';

@NgModule({
  imports: [SharedModule, BoutiqueRoutingModule],
  declarations: [BoutiqueComponent, BoutiqueDetailComponent, BoutiqueUpdateComponent, BoutiqueDeleteDialogComponent],
})
export class BoutiqueModule {}
