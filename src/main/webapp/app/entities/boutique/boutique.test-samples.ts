import { IBoutique, NewBoutique } from './boutique.model';

export const sampleWithRequiredData: IBoutique = {
  id: 62732,
  name: 'Bike c Borders',
  lien: 'Qatar client-server multimedia',
  thematique: 'Incredible Shoes',
};

export const sampleWithPartialData: IBoutique = {
  id: 69136,
  name: 'Upgradable Automotive Soap',
  lien: 'index',
  thematique: 'Toys',
};

export const sampleWithFullData: IBoutique = {
  id: 5745,
  name: 'b Table Bedfordshire',
  lien: 'Seamless b',
  horairesOuverture: 'Agent hard Virtual',
  thematique: 'Royale',
};

export const sampleWithNewData: NewBoutique = {
  name: 'Fully-configurable Nord-Pas-de-Calais',
  lien: 'La',
  thematique: 'strategize Credit',
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
