package polytech.info.gl.domain.enumeration;

/**
 * The Etat enumeration.
 */
public enum Etat {
    En_cours_de_preparation,
    En_cours_de_livraison,
    Prete,
}
