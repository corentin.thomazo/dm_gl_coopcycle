import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IUtilisateur, NewUtilisateur } from '../utilisateur.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IUtilisateur for edit and NewUtilisateurFormGroupInput for create.
 */
type UtilisateurFormGroupInput = IUtilisateur | PartialWithRequiredKeyOf<NewUtilisateur>;

type UtilisateurFormDefaults = Pick<NewUtilisateur, 'id' | 'cooperatives'>;

type UtilisateurFormGroupContent = {
  role: FormControl<IUtilisateur['role']>;
  id: FormControl<IUtilisateur['id'] | NewUtilisateur['id']>;
  name: FormControl<IUtilisateur['name']>;
  adresse: FormControl<IUtilisateur['adresse']>;
  telephone: FormControl<IUtilisateur['telephone']>;
  mail: FormControl<IUtilisateur['mail']>;
  cooperatives: FormControl<IUtilisateur['cooperatives']>;
};

export type UtilisateurFormGroup = FormGroup<UtilisateurFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class UtilisateurFormService {
  createUtilisateurFormGroup(utilisateur: UtilisateurFormGroupInput = { id: null }): UtilisateurFormGroup {
    const utilisateurRawValue = {
      ...this.getFormDefaults(),
      ...utilisateur,
    };
    return new FormGroup<UtilisateurFormGroupContent>({
      role: new FormControl(utilisateurRawValue.role),
      id: new FormControl(
        { value: utilisateurRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      name: new FormControl(utilisateurRawValue.name, {
        validators: [Validators.required],
      }),
      adresse: new FormControl(utilisateurRawValue.adresse, {
        validators: [Validators.required],
      }),
      telephone: new FormControl(utilisateurRawValue.telephone, {
        validators: [Validators.required],
      }),
      mail: new FormControl(utilisateurRawValue.mail, {
        validators: [Validators.required],
      }),
      cooperatives: new FormControl(utilisateurRawValue.cooperatives ?? []),
    });
  }

  getUtilisateur(form: UtilisateurFormGroup): IUtilisateur | NewUtilisateur {
    return form.getRawValue() as IUtilisateur | NewUtilisateur;
  }

  resetForm(form: UtilisateurFormGroup, utilisateur: UtilisateurFormGroupInput): void {
    const utilisateurRawValue = { ...this.getFormDefaults(), ...utilisateur };
    form.reset(
      {
        ...utilisateurRawValue,
        id: { value: utilisateurRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): UtilisateurFormDefaults {
    return {
      id: null,
      cooperatives: [],
    };
  }
}
