package polytech.info.gl.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import polytech.info.gl.domain.Coursier;

/**
 * Spring Data JPA repository for the Coursier entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CoursierRepository extends JpaRepository<Coursier, Long> {}
