import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { CoursierFormService } from './coursier-form.service';
import { CoursierService } from '../service/coursier.service';
import { ICoursier } from '../coursier.model';
import { IUtilisateur } from 'app/entities/utilisateur/utilisateur.model';
import { UtilisateurService } from 'app/entities/utilisateur/service/utilisateur.service';
import { IPanier } from 'app/entities/panier/panier.model';
import { PanierService } from 'app/entities/panier/service/panier.service';

import { CoursierUpdateComponent } from './coursier-update.component';

describe('Coursier Management Update Component', () => {
  let comp: CoursierUpdateComponent;
  let fixture: ComponentFixture<CoursierUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let coursierFormService: CoursierFormService;
  let coursierService: CoursierService;
  let utilisateurService: UtilisateurService;
  let panierService: PanierService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [CoursierUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(CoursierUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CoursierUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    coursierFormService = TestBed.inject(CoursierFormService);
    coursierService = TestBed.inject(CoursierService);
    utilisateurService = TestBed.inject(UtilisateurService);
    panierService = TestBed.inject(PanierService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Utilisateur query and add missing value', () => {
      const coursier: ICoursier = { id: 456 };
      const utilisateur: IUtilisateur = { id: 4057 };
      coursier.utilisateur = utilisateur;

      const utilisateurCollection: IUtilisateur[] = [{ id: 6568 }];
      jest.spyOn(utilisateurService, 'query').mockReturnValue(of(new HttpResponse({ body: utilisateurCollection })));
      const additionalUtilisateurs = [utilisateur];
      const expectedCollection: IUtilisateur[] = [...additionalUtilisateurs, ...utilisateurCollection];
      jest.spyOn(utilisateurService, 'addUtilisateurToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ coursier });
      comp.ngOnInit();

      expect(utilisateurService.query).toHaveBeenCalled();
      expect(utilisateurService.addUtilisateurToCollectionIfMissing).toHaveBeenCalledWith(
        utilisateurCollection,
        ...additionalUtilisateurs.map(expect.objectContaining)
      );
      expect(comp.utilisateursSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Panier query and add missing value', () => {
      const coursier: ICoursier = { id: 456 };
      const panier: IPanier = { id: 77961 };
      coursier.panier = panier;

      const panierCollection: IPanier[] = [{ id: 12923 }];
      jest.spyOn(panierService, 'query').mockReturnValue(of(new HttpResponse({ body: panierCollection })));
      const additionalPaniers = [panier];
      const expectedCollection: IPanier[] = [...additionalPaniers, ...panierCollection];
      jest.spyOn(panierService, 'addPanierToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ coursier });
      comp.ngOnInit();

      expect(panierService.query).toHaveBeenCalled();
      expect(panierService.addPanierToCollectionIfMissing).toHaveBeenCalledWith(
        panierCollection,
        ...additionalPaniers.map(expect.objectContaining)
      );
      expect(comp.paniersSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const coursier: ICoursier = { id: 456 };
      const utilisateur: IUtilisateur = { id: 42683 };
      coursier.utilisateur = utilisateur;
      const panier: IPanier = { id: 79550 };
      coursier.panier = panier;

      activatedRoute.data = of({ coursier });
      comp.ngOnInit();

      expect(comp.utilisateursSharedCollection).toContain(utilisateur);
      expect(comp.paniersSharedCollection).toContain(panier);
      expect(comp.coursier).toEqual(coursier);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ICoursier>>();
      const coursier = { id: 123 };
      jest.spyOn(coursierFormService, 'getCoursier').mockReturnValue(coursier);
      jest.spyOn(coursierService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ coursier });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: coursier }));
      saveSubject.complete();

      // THEN
      expect(coursierFormService.getCoursier).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(coursierService.update).toHaveBeenCalledWith(expect.objectContaining(coursier));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ICoursier>>();
      const coursier = { id: 123 };
      jest.spyOn(coursierFormService, 'getCoursier').mockReturnValue({ id: null });
      jest.spyOn(coursierService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ coursier: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: coursier }));
      saveSubject.complete();

      // THEN
      expect(coursierFormService.getCoursier).toHaveBeenCalled();
      expect(coursierService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ICoursier>>();
      const coursier = { id: 123 };
      jest.spyOn(coursierService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ coursier });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(coursierService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareUtilisateur', () => {
      it('Should forward to utilisateurService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(utilisateurService, 'compareUtilisateur');
        comp.compareUtilisateur(entity, entity2);
        expect(utilisateurService.compareUtilisateur).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('comparePanier', () => {
      it('Should forward to panierService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(panierService, 'comparePanier');
        comp.comparePanier(entity, entity2);
        expect(panierService.comparePanier).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
