package polytech.info.gl.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import polytech.info.gl.domain.Panier;

public interface PanierRepositoryWithBagRelationships {
    Optional<Panier> fetchBagRelationships(Optional<Panier> panier);

    List<Panier> fetchBagRelationships(List<Panier> paniers);

    Page<Panier> fetchBagRelationships(Page<Panier> paniers);
}
